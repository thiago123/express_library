var moment = require('moment');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AuthorSchema = new Schema({

    first_name: {type: String, required: true, max: 100},
    family_name: {type: String, required: true, max:100},
    date_of_birth: {type:Date},
    date_of_death: {type:Date}

});

AuthorSchema
.virtual('name')
.get(function(){
    return this.family_name + ', ' + this.first_name;
});

AuthorSchema
.virtual('url')
.get(function (){
    return '/catalog/author/' + this.id;
});

AuthorSchema
.virtual('lifespan')
.get(function () {
    var birth;
    var death;
    var lifespan;
    if(this.date_of_death){
        birth = moment(this.date_of_birth).format('MMMM Do, YYYY');
        death = moment(this.date_of_death).format('MMMM Do, YYYY');
        lifespan = birth + ' - ' + death;
    }
    else { 
        birth = moment(this.date_of_birth).format('MMMM Do, YYYY');
        death = '';
        lifespan = birth + death;
    }
    
  return lifespan
});

module.exports = mongoose.model('Author', AuthorSchema);